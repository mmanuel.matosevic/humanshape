# Humanshape project guide

This short documentation describes steps necessary to compile and run the 3D human body shape building, manipulation, fitting and evaluation presented in the paper:

**Leonid Pishchulin, Stefanie Wuhrer, Thomas Helten, Christian Theobalt and Bernt Schiele
Building Statistical Shape Spaces for 3D Human Modeling In ArXiv, March 2015**

## Configuration

###Compiling
1. Set MATLAB_HOME in following make files:
```
external/lbfgsb-for-matlab/Makefile
shapemodel/Makefile
evaluation/statQuality/align.mk
evaluation/statQuality/evaluation.mk
```

2. Switch to root directory '/humanshape'

3. In shapemodel/lib/include/bool.h comment this line:
```
enum bool { FALSE = 0, false = 0, TRUE = 1, true = 1 }; 
```

4. In shapemodel/lib/make.m overwrite mex command with:
```
% compile shape model
mex -v -largeArrayDims CFLAGS="\$CFLAGS -std=C99" -output shapepose.mexa64 ... 
    shapepose.cpp Show.cpp NMath.cpp ...
    NRBM.cpp paramMap.cpp CTMesh-30DOF.cpp ...
    -I./lib/nr/ -I./lib/include/
```

5. In shapemodel/lib/make.m run command:
```
make
```

###Getting the models

1. In root directory create and switch to dir experiments

2. Download models:
```
    wget http://datasets.d2.mpi-inf.mpg.de/humanshape/caesar.zip
    wget http://datasets.d2.mpi-inf.mpg.de/humanshape/caesar-norm-wsx.zip
    wget http://datasets.d2.mpi-inf.mpg.de/humanshape/caesar-norm-nh.zip
```

3. Unzip the models
```
    unzip caesar.zip && rm -f caesar.zip
    unzip caesar-norm-wsx.zip && rm -f caesar-norm-wsx.zip
    unzip caesar-norm-nh.zip && rm -f caesar-norm-nh.zip
```

###Getting the fitted meshes

1. In same directory (experiments) download fitted meshes:
```
    wget http://datasets.d2.mpi-inf.mpg.de/humanshape/caesar-fitted-meshes.zip
    wget http://datasets.d2.mpi-inf.mpg.de/humanshape/caesar-norm-wsx-fitted-meshes.zip
    wget http://datasets.d2.mpi-inf.mpg.de/humanshape/caesar-norm-nh-fitted-meshes.zip
```

2. Unzip the fitted meshes
```
    unzip caesar-fitted-meshes.zip && rm -f caesar-fitted-meshes.zip
    unzip caesar-norm-wsx-fitted-meshes.zip && rm -f caesar-norm-wsx-fitted-meshes.zip
    unzip caesar-norm-nh-fitted-meshes.zip && rm -f caesar-norm-nh-fitted-meshes.zip
```

###Running
1. Start matlab using terminal and command:
```
export MESA_LOADER_DRIVER_OVERRIDE=i965; matlab
```

2. Edit file fitting/expParams.m like this and change DIR PATH in p.rootDir:
```
function p = expParams(expidx)
p = [];

% root directory of code
p.rootDir = '<DIR PATH>/humanshape/';

% landmarks used to fit the model
p.landmarksSM = {[p.rootDir 'fitting/landmarksIdxs73.mat']};
% faces for model visualization
p.facesSM = [p.rootDir 'fitting/facesShapeModel.mat'];
% model mesh with skining weights
p.modelMesh = [p.rootDir 'shapemodel/model.dat'];
% root directory for experiments
p.expDir = [p.rootDir 'experiments/'];
% directory to save visualizations
p.figures = '';

switch expidx
    
    case 0
        p.name = 'caesar-norm-nh-fitted-meshes';
        % directory with fitting results
        % e.g. p.fitDir = 'caesar-norm-nh-fitted-meshes';
        % each fitted mesh should be stored as p.fitDir/<mesh-name>/NRD.mat
        p.fitDir = [p.expDir '/' p.name];
        %load initial fitting, if available
        p.initDir = p.fitDir;
        % directory to load existing PCA model from
        p.modelInDir = [p.expDir 'caesar'];
        % directory to save a newly learned PCA model to
        p.modelOutDir = [p.fitDir '/model'];
        % shape and pose fitting prior to non-rigid deformation (NRD)
        p.bInit = true;
        % variants of NRD:
        % -1 - no NRD; 0 - NRD, const weights; 1-3 - NRD, reduce weights
        % p.nrdWidx = 3 leads to best results
        p.nrdWidx = 3;
        % number of shape space parameters
        p.nPCA = 20;
        % color id for visualization
        p.colorIdxs = [6 1];
end

if (isfield(p,'colorIdxs') && ~isempty(p.colorIdxs))
    p.colorName = eval_get_color(p.colorIdxs);
    p.colorName = p.colorName ./ 255;
end
```

3. Change to root dir in matlab and run commands:
```
startup
```
```
demo
```

Matlab code should run now!


