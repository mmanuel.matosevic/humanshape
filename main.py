import pywavefront
import math

from scipy.io import loadmat
from scipy.optimize import minimize, fmin_bfgs, fmin_l_bfgs_b

from evaluation_function import calculate_edges, eval_func
import numpy as np
import plotly.express as px
import pandas as pd


def plot(pts):
    x = []
    y = []
    z = []
    for pt in pts:
        x.append(pt[0])
        y.append(pt[1])
        z.append(pt[2])

    df = pd.DataFrame({
        "X": x,
        "Y": y,
        "Z": z
    })
    fig = px.scatter_3d(df, x='X', y='Y', z='Z')
    fig.show()


def mesh_data(mesh_path):
    """
    load files and extract data
    :return:
    temp_pts, temp_nrm, temp_fce, temp_mar, scan_pts, scan_nrm,
    scan_fce, scan_mar
    """
    HOME = '/home/mm51165/Downloads/'
    mesh = pywavefront.Wavefront(HOME + mesh_path, strict=True, collect_faces=True)

    mesh_vert = mesh.vertices
    mesh_fce = mesh.mesh_list[0].faces
    mesh_nrm = mesh.parser.normals
    return mesh_vert, mesh_fce, mesh_nrm, 0


def tuple_to_list(pts):
    pom_pts = []
    for pt in pts:
        pom_pts.append(list(pt))

    return pom_pts


if __name__ == '__main__':

    """
    Getting data from .obj files
    """
    scan_pts, scan_fce, scan_nrm, scan_mar = mesh_data('SPRING0071DwnSmp627Pts.obj')
    temp_pts, temp_fce, temp_nrm, temp_mar = mesh_data('SPRING0001DwnSmp627Pts.obj')

    Ed = loadmat('/home/mm51165/Downloads/Experiment1/Ed_0.mat')
    Em = loadmat('/home/mm51165/Downloads/Experiment1/Em_0.mat')
    Es = loadmat('/home/mm51165/Downloads/Experiment1/Es_0.mat')
    f = loadmat('/home/mm51165/Downloads/Experiment1/f.mat')
    g = loadmat('/home/mm51165/Downloads/Experiment1/g.mat')

    g = np.array(g['g'])
    g = g.reshape((int(len(g) / 12), 3, 4))
    print("Ed_0", Ed)
    print("Em_0", Em)
    print("Es_0", Es)
    """
    Setting to the appropriate format
    """
    pt1_pom = tuple_to_list(scan_pts)
    pt2_pom = tuple_to_list(temp_pts)

    scan_pts = np.array(pt1_pom)
    temp_pts = np.array(pt2_pom)

    # plot(scan_pts)
    # plot(temp_pts)
    """
    M is list of affine transformations
    Initial guess is unit matrix -> diagonal filled with units
    """
    M = []
    for i in range(np.shape(scan_pts)[0]):
        M.append([[1, 0, 0, 0],
                  [0, 1, 0, 0],
                  [0, 0, 1, 0]])

    M = np.array(M, dtype=np.dtype(float))

    """
    Minimize method only allows vector type so we need to flatten our initial guess
    In evaluation function we then reshape matrix
    """
    M = M.flatten()

    edges = calculate_edges(temp_fce)

    """
    Number of pts in scan
    """
    N = np.shape(scan_pts)[0]

    """
    Change this values to get precise evaluation
    """
    alfa = 1
    beta = math.pow(10, 6)
    gama = math.pow(10, -3)

    # print(eval_func(np.array(M), scan_pts, scan_fce, scan_nrm, scan_mar, temp_pts, temp_fce, temp_nrm, temp_mar, alfa, beta, gama,edges))

    res = minimize(eval_func,
                   x0=np.array(M),
                   method='L-BFGS-B',
                   args=(
                   scan_pts, scan_fce, scan_nrm, scan_mar, temp_pts, temp_fce, temp_nrm, temp_mar, alfa, beta, gama,
                   edges),
                   options={'iprint': 99, 'maxiter': 100}, jac=True)

    M = np.array(res.x)
    M = M.reshape((int(np.shape(M)[0] / 12), 3, 4))

    transformed_pts = []
    for i in range(np.shape(temp_pts)[0]):
        transformed_pts.append(np.dot(temp_pts[i], M[i]))

    transformed_pts = np.array(transformed_pts)
    # plot(transformed_pts)
