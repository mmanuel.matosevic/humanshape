from functools import reduce

import numpy as np
from math import pi, acos, ceil
import torch
from scipy.spatial import KDTree

br = 0


def reshape(M):
    """
    :param M: matrice in [1, 0, 0, 0, 0, 1, 0, 0, 0 ,0 , 1, 0, ...] format
    :return: matrice in [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], ...] format
    """
    if len(np.shape(M)) == 1:
        M = M.reshape((int(np.shape(M)[0] / 12), 3, 4))
        M = [matrice.tolist() for matrice in M]

    return M


def change_values(pt1, pt2):
    if pt1 > pt2:
        pom = pt1
        pt1 = pt2
        pt2 = pom

    return pt1, pt2


def calculate_edges(temp_fce):
    """
    Create a record what vertices make specific edge from a template mesh temp_fce
    :param temp_fce:
    :return:
    rubovi array
    """
    M = np.shape(temp_fce)[0]
    rubovi = []
    rubovi_pom = []
    br = 0
    for i in range(M):
        pt1 = temp_fce[i][0]
        pt2 = temp_fce[i][1]
        pt3 = temp_fce[i][2]
        if (pt1, pt2) not in rubovi_pom and (pt2, pt1) not in rubovi_pom:
            pom1, pom2 = change_values(pt1, pt2)
            rubovi.append((pom1, pom2))
            rubovi_pom.append((pt1, pt2))
            rubovi_pom.append((pt2, pt1))
            br = br + 1

        if (pt1, pt3) not in rubovi_pom and (pt3, pt1) not in rubovi_pom:
            pom1, pom3 = change_values(pt1, pt3)
            rubovi.append((pom1, pom3))
            rubovi_pom.append((pt1, pt3))
            rubovi_pom.append((pt3, pt1))
            br = br + 1

        if (pt2, pt3) not in rubovi_pom and (pt3, pt2) not in rubovi_pom:
            pom2, pom3 = change_values(pt2, pt3)
            rubovi.append((pom2, pom3))
            rubovi_pom.append((pt2, pt3))
            rubovi_pom.append((pt3, pt3))
            br = br + 1

    return rubovi


def data_error(X, temp_pts, scan_pts, scan_nrm):
    """
    :param scan_nrm:
    :param X: 3x4 affine transformation matrix
    :param temp_pts: template points
    :param scan_pts: scan points
    :return: sum of the squared distances between each vertex in the template surface and the example surface
    """

    # print("Calculating Ed")
    if np.shape(temp_pts)[0] != len(X):
        X = np.array(X)
        X = reshape(X)

    Ed = 0
    tree_scan = KDTree(scan_pts)
    tree_temp = KDTree(temp_pts)

    trans_temp_pts = []
    for i in range(np.shape(temp_pts)[0]):
        try:
            pom_pts = np.asmatrix(np.array([[temp_pts[i][0]], [temp_pts[i][1]], [temp_pts[i][2]], [1]]))
            pom_point = np.array(np.matmul(X[i], pom_pts))
            trans_temp_pts.append(pom_point.flatten())
        except Exception as e:
            continue

    tree_temp_trans = KDTree(trans_temp_pts)

    j = 0

    for i in range(np.shape(temp_pts)[0]):
        flag = True
        distance_trans_scan, idx_trans_scan = tree_scan.query(trans_temp_pts[i], k=1)
        distance_scan_trans, idx_scan_trans = tree_temp_trans.query(scan_pts[idx_trans_scan], k=1)


        """
        We consider a point on Transformed vertice (pom variable)
        and a point on scan to be compatible
        if the surface normals at each point are no more than 90◦
        apart
        (so that front-facing surfaces will not be matched to back-facing
        surfaces), and the distance between them is within a threshold (we
        use a threshold of 10 cm in our experiments
        """

        """
        Finding distances and indexes of points that are closest to the given point -> pom_squeeze
        """

        distance_scan, idx_scan = tree_scan.query(temp_pts[j], k=1)
        distance_temp, idx_temp = tree_temp.query(scan_pts[idx_scan], k=1)


        """
        We consider a point on T
        and a point on D to be compatible
        if the surface normals at each point are no more than 90◦
        apart
        (so that front-facing surfaces will not be matched to back-facing
        surfaces)
        """

        """
        One_to_one_corr
        Point is valid only if closest point from trans_temp_pts[i] in scan_pts is also closest in scan_pts[i] to temp_pts (both directions)
        """
        if trans_temp_pts[idx_scan_trans][0] == trans_temp_pts[i][0] and trans_temp_pts[idx_scan_trans][1] == \
                trans_temp_pts[i][1] and trans_temp_pts[idx_scan_trans][2] == trans_temp_pts[i][2]:

            v1_u = temp_pts[j] / np.linalg.norm(temp_pts[j])
            v2_u = scan_nrm[idx_scan] / np.linalg.norm(scan_nrm[idx_scan])
            kut_rad = np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
            kut = np.degrees(kut_rad)

            if distance_scan_trans*100 < 10 and kut < 90:
                squared_dist = distance_scan_trans ** 2 * 100000000

                temp_pts = np.delete(temp_pts, idx_temp, axis=0)
                scan_pts = np.delete(scan_pts, idx_trans_scan, axis=0)

                flag = False

                tree_scan = KDTree(scan_pts)
                tree_temp = KDTree(temp_pts)

                Ed += squared_dist

        if flag == True:
            j += 1

    print("Ed", Ed)
    return Ed


def smoothness_error(X, edges, temp_pts):
    """
    :param X: 4x4 affine transformation matrix
    :param edges: edges of template points
    :param edges: template points
    :return: sum of all affine matrices in frobenius norm squared
    """

    # if np.shape(temp_pts)[0] != len(X):
    #     X = np.array(X)
    #     X = X.reshape((int(len(X) / 12), 3, 4))

    # TODO napraviti bolji algoritam
    # print("Calculating Es")
    Es = 0
    for edge in edges:
        pt1 = temp_pts[edge[0]]
        pt2 = temp_pts[edge[1]]

        pom = np.subtract(X[edge[0]], X[edge[1]])
        fro_norm = np.linalg.norm(pom, ord='fro')
        fro_norm_squared = fro_norm ** 2
        Es += fro_norm_squared

    print("Es", Es)
    return Es


def marker_error(X, scan_pts, temp_mar):
    """
    :param X: 4x4 affine transformation matrix
    :param scan_pts: scan points
    :param temp_mar: anthropometric markers that were placed on the subjects
    :return: Em minimizes the distance between each marker’s location on the template surface
            and its location on the example surface
    """

    Em = 0
    for i in range(np.shape(temp_mar)[0]):
        idx_scan = scan_pts.index(temp_mar[i])
        pom = X[idx_scan] * scan_pts
        fro_norm = np.linalg.norm(pom - temp_mar[i])
        fro_norm_squared = fro_norm ** 2
        Em += fro_norm_squared

    print("EM", Em)
    return Em


def gradient(M, scan_pts, scan_fce, scan_nrm, scan_mar, temp_pts, temp_fce, temp_nrm, temp_mar, alfa, beta, gama,
             edges):
    # if np.shape(temp_pts)[0] != len(M):
    #     M = np.array(M)
    #     M = M.reshape((int(len(M) / 12), 3, 4))

    M = np.array(M)
    M = reshape(M)

    cur_X = []
    for matrice in M:
        # print("MATRICE", matrice)
        # if np.shape(matrice)[0] != 3:
        #     cur_X.append(np.delete(matrice, 2, 0))
        cur_X.append(matrice)

    cur_X = np.array(cur_X)
    rate = 0.01  # learning rate
    precision = 0.000001  # tells us when to stop the algorithm
    previous_step_size = 1
    max_iters = 1  # maximum number of iteration
    iters = 0  # iteration counter
    df = lambda X: alfa * data_error(X, temp_pts, scan_pts, scan_nrm) + beta * smoothness_error(X, edges,
                                                                                                temp_pts)  # gradient of our function

    # while previous_step_size > precision and iters < max_iters:
    while iters < max_iters:
        prev_X = cur_X  # store current x value in prev_X
        cur_X = cur_X - rate * df(M)  # grad descent
        previous_step_size = abs(cur_X - prev_X)  # change in x
        iters = iters + 1  # iteration count

    return cur_X


def eval_func(M, scan_pts, scan_fce, scan_nrm, scan_mar, temp_pts, temp_fce, temp_nrm, temp_mar, alfa, beta, gama,
              edges):
    """
    :param edges: all edges in template mesh
    :param scan_pts: scan points
    :param scan_fce: scan faces
    :param scan_nrm: scan normals
    :param scan_mar: scan markers
    :param temp_pts: template points
    :param temp_fce: template faces
    :param temp_nrm: template normals
    :param temp_mar: template markers
    :param alfa: weighting term to control the influence of data in different region
    :param beta: weighting term to control the influence of data in different region
    :param gama: weighting term to control the influence of data in different region
    :param M: Transformatiom matrix
    :return:
    """

    global br
    br += 1
    if br % 100 == 0:
        print(br)

    M = reshape(M)

    Ed = data_error(M, temp_pts, scan_pts, scan_nrm)
    Es = smoothness_error(M, edges, temp_pts)
    E = alfa * Ed + beta * Es

    g = np.array(
        gradient(M, scan_pts, scan_fce, scan_nrm, scan_mar, temp_pts, temp_fce, temp_nrm, temp_mar, alfa, beta, gama,
                 edges))

    grad = []
    for matrice in g:
        if np.shape(matrice)[0] != 3:
            grad.append(np.delete(matrice, 2, 0))
        else:
            grad.append(matrice)

    grad = np.array(grad)
    return E, grad.flatten()
